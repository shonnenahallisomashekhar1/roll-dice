package com.example.androidweek4

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import com.example.androidweek4.ui.theme.AndroidWeek4Theme
import org.intellij.lang.annotations.JdkConstants.HorizontalAlignment

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            AndroidWeek4Theme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Roller(6)
                }
            }
        }
    }
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
   Row(modifier = modifier.fillMaxWidth(),
       horizontalArrangement = Arrangement.Center,
       verticalAlignment = Alignment.Top)
   {
       Button(onClick = {
           Log.i(this::class.simpleName,">>Clicked")
       }){
           Text("Roll")
       }

   }

}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    AndroidWeek4Theme {
        Greeting("Android")
    }
}


//Roller composable func
@Composable
fun Roller(value: Int, modifier: Modifier = Modifier) {
    var rolled by remember { mutableStateOf(value) }
    Column(modifier = modifier.fillMaxSize()
        .background(Color.LightGray),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
        )
    {
        Text(
            text= if(rolled==0)"please roll" else "$rolled"
        )
        Button(onClick = {
            rolled = (1..6).random()
            Log.i(this::class.simpleName,"rolled $rolled")
        }){
            Text("Roll Dice")
        }

    }

}

